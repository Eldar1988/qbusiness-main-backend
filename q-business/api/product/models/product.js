'use strict';
const services = require('../services/product')
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async afterCreate(result, data) {
      await services.updateOrderSum(result.order?.id)
    },

    async afterUpdate(result, params, data) {
      await services.updateOrderSum(result.order?.id)
    },

    async afterDelete(result, params) {
      await services.updateOrderSum(result.order?.id)
    },
  }
};
