'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

module.exports = {
  updateOrderSum: async (id) => {
    if (id) {
      const order = await strapi.services.order.findOne({id})
      let orderSum = 0
      let orderCostSum = 0
      order.products.forEach(item => {
        orderSum += item.price * item.quantity
        orderCostSum += item.costPrice * item.quantity
      })

      await strapi.services.order.update({id}, { sum: orderSum, costSum: orderCostSum })
    }

  }
};
